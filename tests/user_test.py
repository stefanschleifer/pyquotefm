import unittest

from pyquotefm.user import User

class UserTest(unittest.TestCase):

        def setUp(self):
                self.u = User(username='stefanschleifer')

        def test_user_by_id(self):
                testuser = User(2345)
                self.failUnlessEqual(testuser.id, 2345)
                self.failUnlessEqual(testuser.username, 'theflow')
                self.failUnlessEqual(testuser.created, 'Fri, 06 Jan 2012 13:07:53 +0100')

        def test_user_by_username(self):
                self.failUnlessEqual(self.u.id, 2923)
                self.failUnlessEqual(self.u.created, 'Mon, 20 Feb 2012 18:52:48 +0100')

        def test_list_followers(self):
                followers = self.u.list_followers()
                for f in followers:
                        if f.id==88:
                                return

                self.fail("Either something went wrong or Luca unfollowed me. :(")

        def test_list_followings(self):
                followings = self.u.list_followings()
                for f in followings:
                        if f.id==2527:
                                return
                self.fail("Either something went wrong or I unfollowed chuckpala. :/")

if __name__ == "__main__":
        unittest.main()
