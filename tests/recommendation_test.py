import unittest

from pyquotefm.recommendation import Recommendation

class RecommendationTest(unittest.TestCase):

        def test_recommendation(self):
                r = Recommendation(23)
                self.failUnlessEqual(r.id, 23)
                self.failUnlessEqual(r.user_id, 8)
                self.failUnlessEqual(r.article_id, 14)
                self.failUnlessEqual(r.page_id, 20)
                self.failUnlessEqual(r.category_id, 0)
                self.failUnlessEqual(r.created, 'Thu, 06 Jan 2011 08:19:26 +0100')

        def test_recommendation_user(self):
                r = Recommendation(25)
                u = r.user
                self.failUnlessEqual(u.id, 11)

        def test_recommendation_article(self):
                r = Recommendation(654)
                a = r.article
                self.failUnlessEqual(a.id, 473)

        def test_recommendation_comments(self):
                pass #TODO

if __name__ == "__main__":
        unittest.main()
