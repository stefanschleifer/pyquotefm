import unittest

from pyquotefm.article import Article

class ArticleTest(unittest.TestCase):

        def test_article(self):
                a = Article(12)
                self.failUnlessEqual(a.id, 12)
                self.failUnlessEqual(a.url,
                'http://sethgodin.typepad.com/seths_blog/2010/12/the-first-rule-of-doing-work-that-matters.html')
                self.failUnlessEqual(a.title, 'The first rule of doing work ' \
                'that matters')
                self.failUnlessEqual(a.length, 178)
                self.failUnlessEqual(a.ert, 1)
                self.failUnlessEqual(a.language, 'en')
                self.failUnlessEqual(a.created, 'Wed, 18 Jan 2012 10:43:52 +0100')

if __name__ == "__main__":
        unittest.main()
