import unittest

from pyquotefm.page import Page

class PageTest(unittest.TestCase):

        def test_page(self):
                p = Page(42)
                self.failUnlessEqual(p.id, 42)
                self.failUnlessEqual(p.name, 'iamyourcanadianboyfriend.com')
                self.failUnlessEqual(p.created, 'Thu, 03 Feb 2011 11:28:02 +0100')

if __name__ == "__main__":
        unittest.main()
