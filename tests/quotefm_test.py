import unittest

from pyquotefm.QuoteFM import QuoteFMAPI

class QuoteFMTest(unittest.TestCase):

        def setUp(self):
                self.api = QuoteFMAPI()

        def test_get_recommendation(self):
                r = self.api.get_recommendation(44)
                self.failUnlessEqual(r.id, 44)
                self.failUnlessEqual(r.user_id, 17)
                self.failUnlessEqual(r.user.username, 'hsw')
                self.failUnlessEqual(r.created, 'Fri, 14 Jan 2011 12:49:33 +0100')
                self.failUnlessEqual(r.article.title, 'Die Unbekannte aus der Seine')

        def test_list_recommendations_by_article(self):
                r = self.api.list_recommendations_by_article(19275)
                self.failUnlessEqual(len(r), 13)

        def test_list_recommendations_by_user(self):
                recommendations = self.api.list_recommendations_by_user('stefanschleifer')
                for r in recommendations:
                        if r.id==35800:
                                return

                self.fail("Recommendation 35800 not found for this user.")

        def test_get_article(self):
                a = self.api.get_article(776)
                self.failUnlessEqual(a.length, 318)                

if __name__ == "__main__":
        unittest.main()
