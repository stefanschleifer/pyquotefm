
import urllib2
import json

BASEURL = 'https://quote.fm/api/'

def do_api_call(baseurl, category, method, param = {}):
        url = "%s/%s/%s?" % (baseurl, category, method)
        for k, v in param.iteritems():
                url +="%s=%s&" % (k, v)
        result = urllib2.urlopen(url)
        return json.loads(result.read())
