
from common import do_api_call, BASEURL

class User(object):
        
        id = None
        username = ''
        fullname = ''
        bio = ''
        avatar = ''
        twitter = ''
        location = ''
        url = ''
        created = ''
        follower_count = ''
        following_count = ''

        def __init__(self, id=None, username=None):
        
                u = None

                if id:
                        self.id = id
                        u = do_api_call(BASEURL, 'user', 'get',
                        {'id' : self.id})
                if username:
                        self.username = username
                        u = do_api_call(BASEURL,'user', 'get',
                        {'username' : self.username})

                self.id = u['id']
                self.fullname = u['fullname']
                self.username = u['username']
                self.bio = u['bio']
                self.avatar = u['avatar']
                self.twitter = u['twitter']
                self.location = u['location']
                self.url = u['url']
                self.created = u['created']
                self.follower_count = u['followerCount']
                self.following_count = u['followingCount']

        def get(self):
                return self

        def list_followers(self, page_size=100, page=0):
                f = do_api_call(BASEURL, 'user', 'listFollowers', {'username' :
                self.username, 'pageSize' : page_size, 'page' : page})
                followers = []
                for i in f['entities']:
                        followers.append(User(i['id']))

                return followers

        def list_followings(self, page_size=100, page=0):
                f = do_api_call(BASEURL, 'user', 'listFollowings', {'username' :
                self.username, 'pageSize' : page_size, 'page' : page})
                followings = []
                for i in f['entities']:
                        followings.append(User(i['id']))

                return followings

        def __repr__(self):
                return u"<QuoteFM-API User %s: %s>" % (self.id, self.username)
