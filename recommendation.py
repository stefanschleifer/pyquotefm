
from common import do_api_call, BASEURL

from user import User
from article import Article

class Recommendation(object):
        
        id = None
        user_id = 0
        user = None
        article_id = 0
        page_id = 0
        category_id = 0
        quote = ''
        popularity = ''
        comment_count = 0
        created = ''
        article = None
        comments = []

        def __init__(self, id):
                self.id = id
                r = do_api_call(BASEURL, 'recommendation', 'get', {'id' :
                self.id})
                self.id = r['id']
                self.user_id = r['user_id']
                self.user = User(id=self.user_id)
                self.article_id = r['article_id']
                self.page_id = r['page_id']
                self.category_id = r['category_id']
                self.quote = r['quote']
                self.popularity = r['popularity']
                self.comment_count = r['commentCount']
                self.created = r['created']
                self.article = Article(self.article_id)
                # TODO self.comments

        def get(self):
                return self

        def __repr__(self):
                return u"<QuoteFM-API Recommendation %s>" % self.id
