
from common import do_api_call, BASEURL

class Article(object):
        
        id = None
        url = ''
        title = ''
        length = 0
        ert = 4
        language = ''
        popularity = ''
        recommendation_count = 0
        created = ''

        def __init__(self, id):
                self.id = id
                a = do_api_call(BASEURL, 'article', 'get', {'id' : self.id})
                self.id = a['id']
                self.url = a['url']
                self.title = a['title']
                self.length = a['length']
                self.ert = a['ert']
                self.language = a['language']
                self.popularity = a['popularity']
                self.recommendation_count = a['recommendationCount']
                self.created = a['created']

        def get(self):
                return self

        def __repr__(self):
                return u"<QuoteFM-API Article %s>" % self.id
