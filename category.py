class Category(object):
        
        id = None
        name = ''

        def __init__(self, id, name):
                self.id = id
                self.name = name

        def get(self):
                return self

        def __repr__(self):
                return u"<QuoteFM-API Category %s: %s>" % (self.id, self.name)
