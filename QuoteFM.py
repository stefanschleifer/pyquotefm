
import urllib2
import json

from common import BASEURL

from category import Category
from user import User
from page import Page
from article import Article
from recommendation import Recommendation

class QuoteFMAPI(object):

        baseurl = BASEURL
        api_version = 1.0
        version = 0.1

        def __init__(self, baseurl=None):
                if baseurl:
                        self.baseurl = baseurl

        def _do_api_call(self, category, method, param = {}):
                url = "%s/%s/%s?" % (self.baseurl, category, method)
                for k, v in param.iteritems():
                        url +="%s=%s&" % (k, v)
                result = urllib2.urlopen(url)
                return json.loads(result.read())     

        def get_recommendation(self, id):
                return Recommendation(id)

        def list_recommendations_by_article(self, article_id, scope='time',
        page_size=100, page=0):
                recommendations = self._do_api_call('recommendation',
                'listByArticle', {'id' : article_id, 'scope' : scope,
                'pageSize' : page_size, 'page' : page})
                result = []
                for r in recommendations['entities']:
                        result.append(Recommendation(r['id']))

                return result

        def list_recommendations_by_user(self, username, scope='time',
        page_size=100, page=0):
                recommendations = self._do_api_call('recommendation',
                'listByUser', {'username' : username, 'scope' : scope,
                'pageSize' : page_size, 'page' : page})
                result = []
                for r in recommendations['entities']:
                        result.append(Recommendation(r['id']))

                return result

        def get_article(self, id):
                return Article(id)

        def list_articles_by_page(self, id, scope='time', page_size=100, page=0):
                articles = self._do_api_call('article', 'listByPage', {'id' :
                id, 'scope' : scope, 'pageSize' : page_size, 'page' : page})
                result = []
                for a in articles['entities']:
                        result.append(Article(id=a['id']))

                return result

        def list_articles_by_categories(self, ids=[], language='any',
        scope='time', page_size=100, page=0):
                res = ','.join([str(x) for x in ids])
                articles = self._do_api_call('article', 'listByCategories',
                {'ids' : res, 'language' : language, 'scope' : scope, 'pageSize'
                : page_size, 'page' : page})
                result = []
                for a in articles['entities']:
                        result.append(Article(id=a['id']))

                return result

        def get_page(self, id=None, domain=None):
                if id:
                        return Page(id=id)
                if domain:
                        return Page(domain=domain)

                raise "Either page id or domain must be supplied"

        def list_pages(self, page_size=100, page=0):
                pages = self._do_api_call('page', 'list', {'pageSize' :
                page_size, 'page' : page})
                result = []
                for r in result['entities']:
                        result.append(Page(r['id']))
                        
                return result    

        def get_user(self, id=None, username=None):
                if id:
                        return User(id=id)
                if username:
                        return User(username=username)

                raise "Either user id or username must be supplied"

        def list_categories(self):
                categories = self._do_api_call('category', 'list')
                result = []
                for c in categories['entities']:
                        result.append(Category(c['id'], c['name']))

                return result
