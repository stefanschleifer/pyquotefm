
from common import do_api_call, BASEURL

class Page(object):
        
        id = None
        name = ''
        description = ''
        created = ''

        def __init__(self, id=None, domain=None):
                if id:
                        self.id = id
                        p = do_api_call(BASEURL, 'page', 'get', {'id' :
                        self.id})
                if domain:
                        self.name = domain
                        p = do_api_call(BASEURL, 'page', 'get', {'domain' :
                        self.name})

                self.id = p['id']
                self.name = p['name']
                self.description = p['description']
                self.created = p['created']

        def get(self):
                return self

        def __repr__(self):
                return u"<QuoteFM-API Page %s: %s>" % (self.id, self.name)
